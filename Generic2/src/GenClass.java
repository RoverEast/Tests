/**
 * Created by Администратор on 18.12.2016.
 */
public class GenClass<T extends Number> {

    private T price;

    public GenClass(T price) {
        this.price = price;
    }


    public T getPrice() {
        return price;
    }

    private int roundPrice(){
        return Math.round(price.floatValue());
    }

    public boolean equalsToPrice(GenClass<?> obj){

        return roundPrice() == obj.roundPrice();

    }

}
